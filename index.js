const api = require('./lib/blockonomics')
const fs = require('fs')
const retry = require('async.retry')
const series = require('async.series')
const chunk = require('lodash.chunk')

// load up the addresses to watch
const addresses = fs.readFileSync('./lib/wallets.txt').toString().split('\n')

// api only allows 50 addresses at a time
const address_chunks = chunk(addresses, 50)

// accumulator
let total = 0

const tasks = address_chunks.map(batch => {
    return cb => {
        retry({
            times: 5, 
            interval: 5000
        }, (callback)=>{
            api(batch.join(' '), function(error, result) {
                if(error){
                    console.error('error thrown in API call', error)
                    return callback(error)
                }
                for(let i=0;i<result.length;i++){
                    let res = result[i]
                    if(!res.confirmed){
                        console.log('🚫 bad', res)
                        process.exit(1)
                    }
                    total+=res.confirmed
                }
                callback()
            })    
        }, cb)
    }
})

series(tasks, (err, data)=>{
    console.log(
        err ? '💀' : '✅', 
        `total known reserves: ${total/100000000} BTC`, 
        `average per wallet: ${(total/addresses.length/100000000).toFixed(8)}`
    )
})