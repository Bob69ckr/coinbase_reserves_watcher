const http = require('https');
const agent = require('./agent');

const options = {
    headers: {
        "Origin": "https://bitref.com",
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Mobile Safari/537.36'
    },
    keepAlive: true,
    host: 'blockchain.info',
    path: '/multiaddr?cors=true&active='
};

module.exports = function(addr, cb) {
    options.headers['User-Agent'] = agent();
    options.path = '/balance?cors=true&active='+addr
    console.log('GET', 'https://'+options.host+options.path)
    return http.get(options, function(res) {
        const { statusCode } = res;
        res.setEncoding('utf8');
        // Continuously update stream with data
        let body = '';
        res.on('data', function(d) {
            body += d;
        });
        res.on('end', function() {
            if(statusCode!==200){
                console.error(statusCode, body);
            }
            try{
                let parsed = JSON.parse(body);
                cb(parsed); 
            }catch(e){
                throw(e);
            }
        });
    });
}