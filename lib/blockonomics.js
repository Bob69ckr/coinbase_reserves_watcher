const http = require('https');
const agent = require('./agent');
const options = {
    headers: {
        'Origin': 'https://www.blockonomics.co',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Mobile Safari/537.36',
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'en-US,en;q=0.9',
        'Content-Type': 'application/json;charset=UTF-8',
        'Content-Length': 0,
        'Referer': 'https://www.blockonomics.co/',
        'Connection': 'keep-alive'
    },
    method: 'POST',
    host: 'www.blockonomics.co',
    path: '/api/balance'
};

const balance = module.exports = function(addr, cb) {
    const postData = '{"addr":"'+addr+'"}';
    options.headers['User-Agent'] = agent();
    options.headers['Content-Length'] = postData.length;
    // console.log('curl -d',postData,'https://'+options.host+options.path)
    const req = http.request(options, function(res) {
        const { statusCode } = res;
        res.setEncoding('utf8');
        // Continuously update stream with data
        let body = '';
        res.on('data', function(d) {
            body += d;
        });
        res.on('end', function() {
            // console.log(statusCode, body);
            if(statusCode!==200){
                console.log(statusCode, body);
                return cb({balance:'?',txn:'?',src:'bl'});
            }
            try{
                cb(null, JSON.parse(body).response); 
            }catch(e){
                cb(e);
            }
        });
    });
    req.on('error', (e) => {
        console.error(e);
        return cb({balance:'?',txn:'?',src:'bl'});
    });
    
    req.write(postData);
    req.end();

    return;
}